// ignore_for_file: deprecated_member_use

import 'package:example/view/image/second_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

void main() {
  runApp(const fistscreen());
}

// ignore: camel_case_types
class fistscreen extends StatelessWidget {
  const fistscreen({Key? key}) : super(key: key);
  // ignore: constant_identifier_names
  static const RouteName = 'view/first_screen.dart';
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: const MyHomePage(title: '...'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
            preferredSize: const Size(double.infinity, 180),
            child: AppBar(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  bottomLeft:
                      Radius.circular(10), //định dạng góc bo tròn với vuông
                  bottomRight: Radius.circular(10),
                ),
              ),
              backgroundColor: const Color.fromRGBO(4, 25, 103, 1),
              flexibleSpace: Padding(
                //chia khoang cach
                padding: const EdgeInsets.fromLTRB(90, 50, 70, 0),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 43,
                      right: 150,
                      child: SvgPicture.asset('assets/images/kh2.svg'),
                    ),
                    Positioned(
                      left: 23,
                      bottom: 109.5,
                      child: SvgPicture.asset('assets/images/kh1.svg'),
                    ),
                    Positioned(
                      bottom: 60,
                      child: SvgPicture.asset('assets/images/kh3.svg'),
                    ),
//-------------------------------tiêu đề chữ trên appbar---------------------------
                    Column(
                      children: [
                        const Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 200, 30)),
                        RichText(
                            text: const TextSpan(
                          text: '       Recovered',
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 27,
                            fontStyle: FontStyle.normal,
                          ),
                        )),
                        const Text(
                          "Health",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 27,
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                            wordSpacing: 0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )),

//------------------------Body------------------------------------------
        backgroundColor: const Color.fromRGBO(4, 25, 103, 1),
        body: Container(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(9.5),
                    topRight: Radius.circular(9.5))),
            alignment: Alignment.center,
            child: Center(
                child: Column(
              children: [
                const Padding(padding: EdgeInsets.fromLTRB(51, 30, 52, 0)),
                const Text(
                  "Enter Your Phone Number",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Color.fromRGBO(4, 25, 103, 1),
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 16, 42, 0)),
                RichText(
                    text: const TextSpan(
                        text: 'We will send you the',
                        style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        children: <TextSpan>[
                      TextSpan(
                          text: ' 6 digit ',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            // letterSpacing: 0,
                            // wordSpacing: 0,
                          )),
                      TextSpan(
                          text: 'verification',
                          style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ))
                    ])),
                const Text(
                  "code",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromRGBO(0, 0, 0, 1),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 36, 42, 0)),
                const SizedBox(
                  width: 297,
                  child: TextField(
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.phone),
                      hintText: "Your Phone Number",
                      fillColor: Colors.black,
                    ),
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 26, 42, 0)),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: const Color.fromRGBO(4, 25, 103, 1),
                    onPrimary: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(27.0)),
                    minimumSize: const Size(300, 37),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SecondScreen()),
                    );
                  },
                  child: const Text('NEXT', style: TextStyle(fontSize: 16)),
                ),
              ],
            ))));
  }
}
