// ignore_for_file: deprecated_member_use, unused_import

import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:example/view/image/second_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:async';

void main() {
  runApp(const ThirdScreen());
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);
  static const routeName = 'view/image/third_screen.dart';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: const MyHomePage2(title: '...'),
    );
  }
}

class MyHomePage2 extends StatefulWidget {
  const MyHomePage2({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage2> createState() => _MyHomePageState2();
}

// ignore: unused_element
class _MyHomePageState2 extends State<MyHomePage2> {
  late Timer _timer;
  var _start = 60; // bắt đầu từ 60
  void startTimer() {
    const oneSec = Duration(seconds: 1); // giảm 1 giây
    // đếm lại sau mỗi lần nhấn nút
    if (_start == 0) {
      _timer.cancel();
      _start = 60;
    }
    if (_start == 60 || _start == 0) {
      _timer = Timer.periodic(
        oneSec,
        (Timer timer) {
          if (_start < 1) {
            setState(() {
              timer.cancel();
            });
          } else {
            setState(() {
              _start--;
            });
          }
        },
      );
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
            preferredSize: const Size(double.infinity, 180),
            child: AppBar(
              backgroundColor: const Color.fromRGBO(4, 25, 103, 1),
              flexibleSpace: Padding(
                //chia khoang cach
                padding: const EdgeInsets.fromLTRB(90, 50, 70, 0),
                child: Stack(
                  children: <Widget>[
                    Row(
                      children: const [],
                    ),
                    Positioned(
                      top: 43,
                      right: 150,
                      child: SvgPicture.asset('assets/images/kh2.svg'),
                    ),
                    Positioned(
                      left: 23,
                      bottom: 109.5,
                      child: SvgPicture.asset('assets/images/kh1.svg'),
                    ),
                    Positioned(
                      bottom: 60,
                      child: SvgPicture.asset('assets/images/kh3.svg'),
                    ),
//-------------------------------tiêu đề chữ trên appbar---------------------------
                    Column(
                      children: [
                        const Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 200, 30)),
                        RichText(
                            text: const TextSpan(
                          text: '       Recovered',
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 27,
                            fontStyle: FontStyle.normal,
                          ),
                        )),
                        const Text(
                          "Health",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 27,
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                            wordSpacing: 0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )),
// //------------------------Body------------------------------------------
        backgroundColor: const Color.fromRGBO(4, 25, 103, 1),
        body: Container(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15))),
            alignment: Alignment.center,
            child: Center(
                child: Column(
              children: [
                IconButton(
                    padding: const EdgeInsets.fromLTRB(0, 17, 300, 0),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SecondScreen()),
                      );
                    },
                    icon: const Icon(Icons.arrow_back_ios)),
                const Padding(padding: EdgeInsets.fromLTRB(51, 5, 52, 0)),
                const Text(
                  "OTP Verification",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Color.fromRGBO(4, 25, 103, 1),
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 17, 42, 0)),
                const Text(
                  "A verifiction codes has been sent",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromRGBO(0, 0, 0, 1),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 9, 42, 0)),
                RichText(
                    text: const TextSpan(
                        text: 'to',
                        style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                        children: <TextSpan>[
                      TextSpan(
                          text: ' (091) 123 - 4567 ',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                          )),
                    ])),
                const Padding(padding: EdgeInsets.fromLTRB(51, 0, 42, 5)),
                SizedBox(
                  width: 300,
                  child: PinCodeTextField(
                    textStyle: const TextStyle(
                      color: Color.fromRGBO(4, 25, 103, 1),
                    ),
                    keyboardType: TextInputType.phone,
                    appContext: context,
                    length: 6,
                    onChanged: (value) {
                      // ignore: avoid_print
                      print(value);
                    },
                    pinTheme: PinTheme(
                        shape: PinCodeFieldShape.underline,
                        activeFillColor: Colors.black,
                        inactiveColor: Colors.black12,
                        selectedColor: Colors.blue,
                        activeColor: Colors.black12),
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 5, 42, 0)),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: const Color.fromRGBO(4, 25, 103, 1),
                    onPrimary: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(27.0)),
                    minimumSize: const Size(300, 37),
                  ),
                  onPressed: () {
                    startTimer();
                  },
                  child: const Text('VERIFY & CONTINUE',
                      style: TextStyle(fontSize: 16)),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 25, 42, 0)),
                const Text(
                  "Send agian OTP",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromRGBO(0, 0, 0, 1),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 10, 42, 0)),
                Text(
                  "00:$_start",
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.blue,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 25, 42, 0)),
                const Text(
                  "RESEND CODE",
                  style: TextStyle(
                    fontSize: 16,
                    shadows: [
                      Shadow(color: Colors.blue, offset: Offset(0, -5))
                    ],
                    color: Colors.transparent,
                    decoration: TextDecoration.underline,
                    decorationColor: Colors.blue,
                    decorationThickness: 2,
                  ),
                )
              ],
            ))));
  }
}
