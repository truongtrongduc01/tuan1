// ignore_for_file: deprecated_member_use

// ignore: unused_import

import 'package:example/view/image/third_screen.dart';

import 'package:flutter/material.dart';

import 'package:flutter_svg/svg.dart';

void main() {
  runApp(const SecondScreen());
}

// ignore: must_be_immutable
class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);
  // ignore: constant_identifier_names
  static const RouteName = 'view/image/second_screen.dart';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: const MyHomePage1(title: '...'),
    );
  }
}

class MyHomePage1 extends StatefulWidget {
  const MyHomePage1({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage1> createState() => _MyHomePageState1();
}

// ignore: unused_element
class _MyHomePageState1 extends State<MyHomePage1> {
  String dropdownValue = '(091)';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
            preferredSize: const Size(double.infinity, 180),
            // ignore: sort_child_properties_last
            child: AppBar(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  bottomLeft:
                      Radius.circular(10), //định dạng góc bo tròn với vuông
                  bottomRight: Radius.circular(10),
                ),
              ),
              backgroundColor: const Color.fromRGBO(4, 25, 103, 1),
              flexibleSpace: Padding(
                //chia khoang cach
                padding: const EdgeInsets.fromLTRB(90, 50, 70, 0),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 43,
                      right: 150,
                      child: SvgPicture.asset('assets/images/kh2.svg'),
                    ),
                    Positioned(
                      left: 23,
                      bottom: 109.5,
                      child: SvgPicture.asset('assets/images/kh1.svg'),
                    ),
                    Positioned(
                      bottom: 60,
                      child: SvgPicture.asset('assets/images/kh3.svg'),
                    ),
//-------------------------------tiêu đề chữ trên appbar---------------------------
                    Column(
                      children: [
                        const Padding(
                            padding: EdgeInsets.fromLTRB(0, 0, 200, 30)),
                        RichText(
                            text: const TextSpan(
                          text: '       Recovered',
                          style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 27,
                            fontStyle: FontStyle.normal,
                          ),
                        )),
                        const Text(
                          "Health",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 27,
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )),
//------------------------Body------------------------------------------
        backgroundColor: const Color.fromRGBO(4, 25, 103, 1),
        body: Container(
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(9.5),
                    topRight: Radius.circular(9.5))),
            alignment: Alignment.center,
            child: Center(
                child: Column(
              children: <Widget>[
                const Padding(padding: EdgeInsets.fromLTRB(51, 30, 52, 0)),
                const Text(
                  "Enter Your Phone Number",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Color.fromRGBO(4, 25, 103, 1),
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 16, 42, 0)),
                RichText(
                    textAlign: TextAlign.center,
                    text: const TextSpan(
                        text: 'We will send you the',
                        style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          // letterSpacing: 0,
                          // wordSpacing: 0,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text: ' 6 digit ',
                              style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                // letterSpacing: 0,
                                // wordSpacing: 0,
                              )),
                          TextSpan(
                              text: 'verification',
                              style: TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ))
                        ])),
                const Text(
                  "code",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromRGBO(0, 0, 0, 1),
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                    wordSpacing: 3,
                  ),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 36, 42, 0)),
                Stack(
                  children: [
                    const Positioned(
                      right: 32,
                      bottom: 38,
                      child: SizedBox(
                        width: 299,
                        child: TextField(
                          // padding: EdgeInsets.fromLTRB(51, 36, 42, 0),
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.local_phone_outlined),
                            prefixText: '              |  ',
                            fillColor: Colors.white60,
                          ),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Color.fromRGBO(4, 25, 103, 1),
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(73, 0, 224, 40),
                        child: DropdownButtonFormField<String>(
                          decoration:
                              const InputDecoration(border: InputBorder.none),
                          value: dropdownValue,
                          icon: const Icon(Icons.arrow_drop_down),
                          style: const TextStyle(
                            color: Color.fromRGBO(4, 25, 103, 1),
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                          ),
                          onChanged: (String? newValue) {
                            setState(() {
                              dropdownValue = newValue!;
                            });
                          },
                          items: <String>[
                            '(091)',
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
                const Padding(padding: EdgeInsets.fromLTRB(51, 0, 42, 0)),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: const Color.fromRGBO(4, 25, 103, 1),
                    onPrimary: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(27.0)),
                    minimumSize: const Size(300, 37),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ThirdScreen()),
                    );
                  },
                  child: const Text('NEXT', style: TextStyle(fontSize: 16)),
                ),
              ],
            ))));
  }
}
